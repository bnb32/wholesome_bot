import numpy as np
import joblib
from phrase_lists import blacklist
import Settings as cfg
from Preprocessing import correctMsg

class WholesomeCheck:
    def __init__(self):
        #self.vec=joblib.load(cfg.VECTORIZER_FILE)
        self.model=joblib.load(cfg.MODEL_FILE)
        self.words=blacklist

    def _get_profane_prob(self,prob):
        return prob[1]

    def predict(self,texts):
        tmp=[correctMsg(t) for t in texts]
        return self.model.predict(tmp)#self.vec.transform(tmp))

    def filter_words(self,texts):
        tmp=texts.lower()
        for w in self.words:
            if w in tmp:
                return True
    
    def predict_prob(self,texts):
        #tmp=[t for t in texts]
        tmp=[correctMsg(t) for t in texts]
        return np.apply_along_axis(self._get_profane_prob, 1, 
                                   self.model.predict_proba(tmp))#self.vec.transform(tmp)))
