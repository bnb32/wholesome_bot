import Settings as cfg 
import numpy as np
import time,re,os
from datetime import datetime
import Preprocessing as pp
import WholesomeCheck as wc
import clean_info as cinfo
from emoji import demojize


wc=wc.WholesomeCheck()

class Info:
    def __init__(self,line):
        self.line = line
        self.user = self.getUser(line)
        self.msg = demojize(self.getMessage(line))
        self.sub = self.isSub(line)
        self.mod = self.isMod(line)
        self.vip = self.isVip(line)
        self.partner = self.isPartner(line)
        self.pleb = self.isPleb(line)
        self.badges = self.getBadges(line)
        self.msgId = self.getMsgId(line)
        self.role = self.getRole(line)
        self.deleted = False

    def getMsgId(self,line):
        for l in line.split(';'):
            if l.startswith('id='): 
                msgId=re.search('id=(.*)',l).group(1)
        return msgId

    
    def getProb(self,msg):
        prob=round(wc.predict_prob([msg])[0],3)
        if wc.filter_words(msg):
            prob+=0.1
        return min(1.0,prob)
            
    def getUser(self,line):
        for l in line.split(';'):
            if l.startswith('user-type='):
                msg=re.search('user-type=(.*)',l).group(1)
        return msg.split(":", 2)[1].split("!", 1)[0]
    
    def getRole(self,line):
        if self.isMod(line): return "mod"
        if self.isVip(line): return "vip"
        if self.isSub(line): return "sub"
        if self.isPleb(line): return "pleb"
        if self.isPartner(line): return "partner"

    def isSub(self,line):
        sub=re.findall(r'\d+',re.search('subscriber=\d+',line).group(0))[0]
        if sub=='1':
            return True
        else:
            return False
    
    def isMod(self,line):
        mod=re.findall(r'\d+',re.search('mod=\d+',line).group(0))[0]
        if "broadcaster" in self.getBadges(line) or mod=='1':
            return True
        else:
            return False
    
    def isPartner(self,line):
        if "partner" in self.getBadges(line):
            return True
        else:
            return False

    def isVip(self,line):
        if "vip" in self.getBadges(line):
            return True
        else:
            return False

    def isPleb(self,line):
        if not self.isVip(line) and not self.isMod(line) and not self.isSub(line):
            return True
        else:
            return False
    
    def getBadges(self,line):
        for l in line.split(';'):
            if l.startswith('badges='):
                badges=re.search('badges=(.*)',l).group(1)
        return badges
    
    def getMessage(self,line):
        for l in line.split(';'):
            if l.startswith('user-type='):
                msg=re.search('user-type=(.*)',l).group(1)
        return re.search('PRIVMSG #\w+ :(.*)',msg).group(1)

class replies:
    def __init__(self):
        self.timeout="/w @%s Hi, I'm the AI running in the background on drchessgremlin's account. I time out messages based on the pattern I have learned from previous chat data. It seems your message matched that pattern. Please refrain from posting similar messages in the future. If you think my pattern detection needs work please whisper drchessgremlin."
        self.bio="/me I'm an AI developed by drchessgremlin, learning what is wholesome and non-wholesome based on what you write in chat."
        self.permit="/me @%s will not be timed out for %s seconds" 
        self.pmsg_usr="/me %s: non-wholesome p=%s"
        self.pmsg="non-wholesome p=%s"
        self.link_msg="/me Please ask a moderator before posting a link"

class Moderation:
    def __init__(self):
        self.mem=pp.msgMemory()
        self.reps=replies()
        self.permits=dict()
        self.pleb_prob=cfg.PLEB_PMIN
        self.sub_prob=cfg.SUB_PMIN
        self.nickname=cfg.NICK
        self.send_pmsg=cfg.PMSG_ON
        self.to_subs=cfg.TO_SUBS
        self.print_info=cfg.PRINT_INFO
        self.nolinks=cfg.NOLINKS
        self.exempt_messages=cinfo.whitelist
        self.exempt_users=['streamelements','moobot']
        self.permit_duration=180
        self.timeout_duration=5

    def updateLine(self,line):
        self.line=line
        return self

    def printInfo(self,info):
        badge=''
        if info.mod: badge="mod"
        elif info.vip: badge="vip"
        elif info.sub: badge="sub"
        elif info.partner: badge="partner"
        else: badge="pleb"
        print("\n(%s) %s: %s (%s)"%(info.badges,info.user,info.msg,info.prob))

    def isAllowedMsg(self,line):
        for msg in self.exempt_messages:
            if msg.lower() in line.lower():
                return True
        return False            
    
    def addPermit(self,user):
        if user not in self.permits.keys():
            self.permits[user]=time.time()
        return self
    
    def delPermit(self,user):
        if user in self.permits:
            self.permits.pop(user)
        return self
    
    def hasPermit(self,user):
        if user in self.permits.keys():
            return True
        else:
            return False
    
    def isExempt(self,info):
        if self.hasPermit(info.user) or info.mod or info.vip or info.partner or (info.sub and not self.to_subs) or self.isAllowedMsg(info.msg):
            return True
        else:
            return False
    
    def timePermit(self,user):
        if self.hasPermit(user):
            if int(time.time())>=(int(self.permits[user])+int(self.permit_duration)):
                self.delPermit(user)
        return self

    def getPermitUser(self,msg):
        user=msg.split()
        user=user[1].lstrip('@')
        return user.lower()
    
    def repPermit(self,info):
        if info.mod and "!permit" in info.msg:
            user=self.getPermitUser(info.msg)
            self.addPermit(user)
            return True
        else:
            self.timePermit(info.user)
            return False
    
    def sendReply(self,s,info):
        #join check response
        #info cmd response
        if "!%s"%(self.nickname) in info.msg:
            pass
            #self.sendMessage(s,self.reps.bio)
        
        #permit response
        elif self.repPermit(info):
            pass
            #self.sendMessage(s,self.reps.permit%(self.getPermitUser(info.msg),self.permit_duration))
        
        #non-wholesome response
        elif self.send_pmsg:
            if self.isProbExceeded(info): 
                pass
                #self.sendMessage(s,self.reps.pmsg_usr%(info.user,info.prob))
            
    def getTimeout(self,user,time,msg):
        act="/timeout @%s %s %s"%(user,time,msg)
        return act
    
    def getDelete(self,info):
        act="/delete %s"%(info.msgId)
        return act

    def isProbExceeded(self,info):
        if self.isExempt(info):
            return False
        elif info.prob>self.pleb_prob and info.pleb:
            return True
        elif info.prob>self.sub_prob and info.sub:
            return True
        else:
            return False
    
    def sendAction(self,s,info):
        #timeout
        if self.isProbExceeded(info):
            
            info.deleted=True
            #update memory with ban
            self.mem.updateUserBan(info.user)
            
            if not self.send_pmsg:
                rep=''
            else:
                rep=self.reps.pmsg%(info.prob)
            #act=self.getTimeout(info.user,'%s'%self.timeout_duration,rep)
            act=self.getDelete(info)
            self.sendMessage(s,act)

            print("\n**%s: %s**\n" %(info.user,info.msg))

        elif pp.containsLink(info.msg) and self.nolinks and not isExempt(info):
            rep=self.reps.link_msg
            #act=self.getTimeout(info.user,'%s'%self.timeout_duration,rep)
            act=self.getDelete(info)
            self.sendMessage(s,act)
    
    def getInfo(self,line):
        info=Info(line)
        info.msg = demojize(self.mem.chunkRecent(info))
        tmp = info.msg
        for w in cinfo.whitelist:
            tmp = re.sub(w,'',tmp,flags=re.I)
        info.prob = info.getProb(tmp)
        return info

    
    def sendMessage(self, s, message):
        messageTemp="PRIVMSG #"+cfg.CHAN+" :"+message
        tmp=messageTemp+"\r\n"
        s.send(tmp.encode("utf-8"))
        print("Sent: " + messageTemp)
