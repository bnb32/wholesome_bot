import os,sys
import requests
import Settings as cfg

client_id=cfg.CLIENT_ID 
client_secret=cfg.CLIENT_SECRET

authURL = "https://id.twitch.tv/oauth2/token"

AutParams = {
             'client_id': client_id,
             'client_secret': client_secret,
             'grant_type': 'client_credentials'
            }

AutCall = requests.post(url=authURL, params=AutParams) 
access_token = AutCall.json()['access_token']

userURL="https://api.twitch.tv/helix/users?login=%s"
channelURL="https://api.twitch.tv/helix/users?login=botezlive"

head = {
        'Client-ID' : client_id,
        'Authorization' :  "Bearer " + access_token
       }

def getUserID(user):
    r = requests.get(userURL %(user), headers = head).json()['data']
    return r[0]['id']
