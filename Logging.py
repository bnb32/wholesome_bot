import re,os
import clean_info as cinfo
from Moderation import wc
from datetime import datetime
import Settings as cfg

class Logging():
    def __init__(self):

        self.UserLog = {}
        tmp='./data/logs/%s_#%s.log'%(datetime.now().strftime("%b-%d-%Y"),cfg.CHAN)
        os.system('mkdir -p ./data/logs')
        if os.path.exists(tmp): 
            write_append = 'a'
        else:
            write_append = 'w'

        self.log_file=open(tmp,write_append)
        self.write_log = cfg.WRITE_LOG
        self.print_connect_info = False
        self.print_log_info = True

    def printConnectInfo(self,text):
        if self.print_connect_info:
            print('\n%s' %text)
        return    

    def printLogInfo(self,text):
        if self.print_log_info:
            print('\n%s' %text)
        return

    def appendLog(self,line):
        if not isVip(line) and not isMod(line):
            line = line.replace('\r','')
            self.log_file.write(line+'\n')
            self.log_file.flush()

def getLineType(line):
    if "CLEARCHAT" in line: 
        return "ban"
    elif "CLEARMSG" in line: 
        return "delete"
    elif "JOIN" in line: 
        return "join"
    elif "PART" in line: 
        return "part"
    elif "PRIVMSG" in line: 
        return "msg"
    else:
        return "misc"

def isMsg(line):
    if getLineType(line) == "msg":
        return True
    else:
        return False
    
def isDelete(line):
    if getLineType(line) == "delete":
        return True
    else:
        return False

def isBan(line):
    if getLineType(line) == "ban":
        return True
    else:
        return False

def getMsgId(line):
    if isMsg(line):    
        for l in line.split(';'):
            if l.startswith('id='): 
                msgId=re.search('id=(.*)',l).group(1)
    elif isDelete(line):
        for l in line.split(';'):
            if l.startswith('target-msg-id='): 
                msgId=re.search('target-msg-id=(.*)',l).group(1)
    return msgId
    
def getUser(line):
    if isMsg(line):    
        for l in line.split(';'):
            if l.startswith('user-type='):
                msg=re.search('user-type=(.*)',l).group(1)
                user=msg.split(":", 2)[1].split("!", 1)[0]
    elif isDelete(line):
        l = line.split(';')
        user = l[0].strip('@login=')
        user = user.replace('\r','').replace('\n','')
    elif isBan(line):
        l = line.split(':')
        user = l[-1].strip()
        user = user.replace('\r','').replace('\n','')
    return user

def isSub(line):
    if isMsg(line):
        sub=re.findall(r'\d+',re.search('subscriber=\d+',line).group(0))[0]
        if sub=='1':
            return True
    return False
    
def isMod(line):
    if isMsg(line):    
        mod=re.findall(r'\d+',re.search('mod=\d+',line).group(0))[0]
        if "broadcaster" in getBadges(line) or mod=='1':
            return True
    return False

def isVip(line):
    if isMsg(line):    
        if "vip" in getBadges(line):
            return True
        
    return False

def isPleb(line):
    if isMsg(line):    
        if not isVip(line) and not isMod(line) and not isSub(line):
            return True
        
    return False
    
def getBadges(line):
    if isMsg(line):    
        for l in line.split(';'):
            if l.startswith('badges='):
                badges=re.search('badges=(.*)',l).group(1)
    else:
        badges = ''

    return badges
    
def getProb(line):
    tmp = getMessage(line)
    for w in cinfo.whitelist:
        tmp = re.sub(w,'',tmp,flags=re.I)
    prob=round(wc.predict_prob([tmp])[0],3)
    if wc.filter_words(tmp):
        prob+=0.1
    return min(1.0,prob)


