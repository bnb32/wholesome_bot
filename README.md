# wholesome\_bot
An ML based chatbot for detecting and filtering offensive twitch chat content

After cloning, copy `SettingsTemplate.py` to `Settings.py` and update variables in `Settings.py` and `clean_info.py`

Required python packages:
`progressbar2`
`pandas`
`nltk`
`scikit-learn`
`numpy`
`gensim`
`notify_run`
'symspellpy`
