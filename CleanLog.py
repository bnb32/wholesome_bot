import Preprocessing as pp
import os,sys,re,time
import progressbar
import numpy as np
import argparse
import Settings as cfg
import WholesomeCheck as wc

if os.path.exists(cfg.MODEL_FILE): # and os.path.exists(cfg.VECTORIZER_FILE):
    wc = wc.WholesomeCheck()

parser=argparse.ArgumentParser(description='Process data for training')
parser.add_argument('-infile',required=True,type=str)
parser.add_argument('-outfile',required=True,type=str)
parser.add_argument('-clean',default=False,action='store_true')
parser.add_argument('-filt',default=False,action='store_true')
parser.add_argument('-trim',default=False,action='store_true')
parser.add_argument('-links',default=False,action='store_true')
parser.add_argument('-spell',default=False,action='store_true')
parser.add_argument('-check_phrases',default=False,action='store_true')
parser.add_argument('-check_probs',default=False,action='store_true')
parser.add_argument('-bounds','--bounds',nargs=2,type=int)
parser.add_argument('-multiplier',default=cfg.MULTIPLIER,type=int)
args=parser.parse_args()

if args.clean:
    #clean log
    cl=pp.LogCleaning(args.infile,args.outfile,wc)
    #mem=pp.msgMemory()

    #lines=cl.prepLog()

    #mem.buildFullMemory(lines)
    #print(mem.memory)
    
    cl.cleanLog()

if args.filt:
    #filter log
    texts,y=pp.readData(args.infile)
    print("Filtering Log")
    with progressbar.ProgressBar(max_value=len(texts)) as bar:
        for n,t in enumerate(texts):
            tmp=pp.filterEmotes(t)
            tmp=re.sub('[^A-Za-z0-9 ]+','',tmp)
            tmp=pp.removeReps(tmp,pp.sym_spell.words)
            #tmp=pp.segmentWords(tmp)
            #tmp=' '.join(pp.my_lemmatizer(tmp.split()))
            texts[n]=tmp
            bar.update(n)
    pp.writeData(args.outfile,texts,y)    

if args.trim:
    #trim log
    
    texts,y=pp.readData(args.infile)
    
    texts=list(texts)
    y=list(y)
    
    znum=y.count(0)
    onum=y.count(1)
    
    frac=args.multiplier*onum/znum
    
    print("Before trim")
    print("Number of zeros: %s"%znum)
    print("Number of ones: %s"%onum)
    
    tnew,ynew=pp.trimLog(texts,y,frac)
    
    print("After trim")
    print("Number of zeros: %s"%ynew.count(0))
    print("Number of ones: %s"%ynew.count(1))
    
    pp.writeData(args.outfile,tnew,ynew)

if args.links:
    #label links
    
    texts,y=pp.readData(args.infile)
    links=[]
    nolinks=[]
    yl=[]
    ynl=[]
    for n,t in enumerate(texts):
        if pp.containsLink(t):
            links.append(t)
            yl.append('1')
        else:
            nolinks.append(t)
            ynl.append(y[n])
    pp.writeData(args.outfile,nolinks,ynl)  
    pp.writeData(args.outfile[0:-4]+'_links.csv',links,yl)  

if args.spell:
    #correct spelling

    texts,y=pp.readData(args.infile)
    print("Correcting spelling")
    with progressbar.ProgressBar(max_value=len(texts)) as bar:
        for n,t in enumerate(texts):
            texts[n]=pp.correctMsg(t)
            bar.update(n)
    pp.writeData(args.outfile,texts,y)
    
if args.check_phrases:
    #reclassify phrases
    pp.checkPhrases(args.infile,args.outfile)
    
if args.check_probs:
    #reclassify phrases
    pp.checkProbs(args.infile,args.outfile,args.bounds,wc)
