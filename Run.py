import string
import Moderation as mod
import Socket as sock
import Settings as cfg
import time
import Logging as log
from notify_run import Notify
notify = Notify()

s = sock.openSocket()
sock.joinRoom(s)
last_ping=time.time()
mod=mod.Moderation()
#from pynput import keyboard as kb
#import KeyHandling as kh
#kh=kh.KeyHandling(mod,s)

#with kb.Listener(on_press=kh.on_press,on_release=kh.on_release) as listener:
#    while kh.break_program==False:
while True:        
        try:
            line=s.recv(1024).decode("utf-8")
            #keep connection
            if line==sock.ping_msg:
                s.send(sock.pong_out_msg.encode("utf-8"))
                last_ping=time.time()
            elif sock.pong_in_msg in line:
                last_ping=time.time()
            
            else:
                #get info on line
                info=mod.getInfo(line)
                if mod.print_info:
                    mod.printInfo(info)

                #response to line
                mod.sendReply(s,info)
                mod.sendAction(s,info)
                
                #write to log
                if mod.write_log:
                    mod.appendLog(log.LogLine(line))
                               
        except:
            if time.time()>=(last_ping+300):
                try:
                    s.send(sock.ping_msg.encode("utf-8"))
                except:
                    print("\n**Lost connection**")
                    notify.send("Chatbot disconnnected")
                    exit()
    #listener.join()
    #mod.destroy()    
