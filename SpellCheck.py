import gensim
import re
from collections import Counter, defaultdict
from metaphone import doublemetaphone as meta
import enchant

model=gensim.models.KeyedVectors.load_word2vec_format('./data/GoogleNews-vectors-negative300.bin',binary=True)

words=model.index2word

w_rank={}
w_meta={}
for i,word in enumerate(words):
    w_rank[word]=i
    w_meta[word]=meta(word)[0]

def invert_dict(d):
    inv=defaultdict(list)
    return {inv[v].append(k) for k,v in d.items()}

METAS=invert_dict(w_meta)
WORDS=w_rank

def words(text): return re.findall(r'\w+',text.lower())

def P(word):
    return - WORDS.get(word,0)

def meta_candidates(word):
    return METAS.get(meta(word)[0],None)

def correction(word):
    return max(candidates(word),key=P)

def candidates(word):
    return (known([word]) or known(edits1(word)) or known(edits2(word)) or [word])

def known(words):
    return set(w for w in words if w in WORDS)

def edits1(word):
    letters='abcdefghijklmnopqrstuvwxyz'
    splits=[(word[:i],word[i:]) for i in range(len(word)+1)]
    deletes=[L+R[1:] for L,R in splits if R]
    transposes=[L+R[1]+R[0]+R[2:] for L,R in splits if len(R)>1]
    replaces=[L+c+R[1:] for L,R in splits if R for c in letters]
    inserts=[L+c+R for L,R in splits for c in letters]
    return set(deletes+transposes+replaces+inserts)

def edits2(word):
    return (e2 for e1 in edits1(word) for e2 in edits1(e1))
