import pandas as pd
import re
import time
import datetime
from emoji import demojize

with open("./data/botezlive.log",'r') as f:
    lines = f.readlines()

def process_lines_dictionary(lines):
    
    udict = {}
    total_count = 0

    print('Building User Dictionary')
    
    for i, line in enumerate(lines):
        sub = mod = vip = deleted = banned = moderated = 0
        user = msg = moderator = ''
        
        start_time = time.time()
        
        if line.startswith('<'):
            user = line.split('>')[0]
            user = user.strip('<')
            msg = line[line.index('>')+1:].lstrip().strip('\n')
        
            if '%' in user: sub = 1
            
            if '@' in user: mod = 1
            
            if '!' in user: vip = 1
        
            user = re.sub('[^A-Za-z0-9_]+','',user).lower()
            
            if user in udict:
                msg_number = len(udict[user])-1
            else:
                msg_number = -1
            
            entry = {'message':demojize(msg),
                     'message number':msg_number+1,
                     'sub':sub,
                     'vip':vip,
                     'mod':mod,
                     'moderator':moderator,
                     'moderated':moderated,
                     'deleted':deleted,
                     'banned':banned,
                     'raw text':line}
            
            total_count += 1

            if user not in udict:
                udict[user] = [entry]

            else:
                udict[user].append(entry)
            
        elif line.startswith('BAN:'):
            try:
                user = line.split()[1].lower()              
                if user in udict:
                    udict[user][-1]['banned'] = 1
                    udict[user][-1]['moderated'] = 1
                    
            except: pass
            
        elif line.startswith('DELETED:'):
            try:
                user = line.split()[1].lower()
                msg = ' '.join(line.split()[2:-1]).strip('\n')
                msg = demojize(msg)
                if user in udict:
                    udict[user][-1]['deleted'] = 1
                    udict[user][-1]['moderated'] = 1
                
            except: pass
            
        elif line.startswith('MOD_ACTION:'):
            try:
                user = line.split()[3].lower()
                moderator = line.split()[1].lower()
                if user in udict:
                    udict[user][-1]['moderator'] = moderator
            
            except: pass
        
        end_time = time.time()
        elapsed = end_time - start_time
        
        remaining_seconds = elapsed*(len(lines)-i-1)
        print("Done: {:.5f}, Remaining: {:<25}".format((i+1)/len(lines),str(datetime.timedelta(seconds=remaining_seconds))),end="\r")
        
    print('\nBuilding User DataFrame')
    
    count = 0
    dict_list = []
    
    for user in udict:
        for entry in udict[user]:

            start_time = time.time()
            
            tmp = entry
            tmp['user'] = user
            dict_list.append(tmp)
            
            end_time = time.time()
            elapsed = end_time - start_time
            
            remaining_seconds = elapsed*(total_count-count-1)
            print("Done: {:.5f}, Remaining: {:<25}".format((count+1)/total_count,str(datetime.timedelta(seconds=remaining_seconds))),end="\r")
            
            count += 1
    
    df = pd.DataFrame(dict_list,
                      columns=['user',
                               'message',
                               'message number',
                               'sub',
                               'vip',
                               'mod',
                               'moderator',
                               'moderated',
                               'deleted',
                               'banned',
                               'raw text'])

    return df


df_messages = process_lines_dictionary(lines)
df_messages.to_csv('./data/botezlive_dataframe.csv',index=False)
