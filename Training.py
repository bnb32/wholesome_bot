import pandas as pd
import numpy as np
import sklearn.feature_extraction.text as ft
import sklearn.calibration as cal
import sklearn.svm as svm
import sklearn.ensemble as ens
import joblib as jl
import Settings as cfg
from sklearn.model_selection import train_test_split,GridSearchCV
from gensim.models import Word2Vec
from gensim.models import KeyedVectors
from sklearn.metrics import confusion_matrix,precision_score,recall_score
from sklearn.metrics import roc_auc_score,log_loss,jaccard_score,f1_score
import Vectorizers as vs
import re, sys
import nltk
import Preprocessing as pp
from nltk.corpus import stopwords
from sklearn.pipeline import Pipeline
import argparse
stop_words=set(stopwords.words('english'))

parser=argparse.ArgumentParser(description="Train model on chat data")
parser.add_argument('-model',required=True)
parser.add_argument('-vectorizer',required=True)
parser.add_argument('-dataset',required=True)
args=parser.parse_args()

model_num=args.model
vec_num=args.vectorizer
data_file=args.dataset

models=dict({1:'linearsvm',2:'extratrees'})
vecs=dict({1:'tfidf',2:'tfidf-w2v',3:'tfidf-glove',4:'mean-w2v',5:'mean-glove',6:'d2v'})

params = dict({'stop_words':None,#stop_words,
               'tokenizer':None,'cv':5,'method':'sigmoid',
               'min_df':1, 'max_df':1.0,'analyzer':'char_wb', 
               'ngram_range':(1,8),'smooth_idf':1, 'sublinear_tf':1})

vparams=dict({'stop_words':params['stop_words'], 'tokenizer':params['tokenizer'], 
              'min_df':params['min_df'],'max_df': params['max_df'],
              'analyzer':params['analyzer'], 
              'ngram_range':params['ngram_range'],
              'smooth_idf':params['smooth_idf'], 
              'sublinear_tf':params['sublinear_tf']}) 

cparams=dict({'max_iter':1000, 'C':1})

w2v_vparams=dict({'stop_words':stop_words, 'tokenizer':pp.my_tokenizer, 
                  'min_df':3, 'analyzer':'word', 'ngram_range':(1,1),
                  'smooth_idf':1, 'sublinear_tf':1, 'max_df': 0.9})

wparams=dict({'size':10,'window':5,'min_count':1,
              'workers':4,'sg':1, 'iter':10, 'hs':1})

gparams=dict({'window':10,'no_components':500,
              'learning_rate':0.05,'epochs':100,
              'no_threads':4})

dparams=dict({'vector_size':5, 'window':2, 
              'min_count':1, 'workers':4})

# Vectorize the text
def get_vectorizer(texts):
    print('Fitting vectorizer')
    print("Using vectorizer: %s"%vecs[int(vec_num)])
    if vec_num=='1':
        vec = ft.TfidfVectorizer(**vparams)
    elif vec_num=='2':
        vec = vs.TfidfVectorizerW2V(w2v_vparams,wparams)
    elif vec_num=='3':
        vec = vs.TfidfVectorizerGlove(w2v_vparams,gparams)
    elif vec_num=='4':
        vec = vs.VectorizerW2V(wparams)
    elif vec_num=='5':
        vec = vs.VectorizerGlove(gparams)
    elif vec_num=='6':
        vec = vs.VectorizerD2V(dparams)
    else:
        print("specify valid vectorizer")
        exit()
    vec.fit(texts)
    return vec

def grid_search(X,y,model):
    
    print('Parameter sweep')
    plist=np.arange(0.4,1.0,0.01).tolist()
    param_grid={'C': plist}
    clf_grid=GridSearchCV(model,param_grid,verbose=1,cv=5,n_jobs=-1)
    clf_grid.fit(X,y)
    print("Best Parameters:\n",clf_grid.best_params_)
    print("Best Estimators:\n",clf_grid.best_estimator_)
    return clf_grid

def train_model(X,y):
  
    print('Training model')
    print("Using classifier: %s"%models[int(model_num)])
    if model_num=='1': 
        clf=svm.LinearSVC(**cparams)
    elif model_num=='2': 
        clf=ens.ExtraTreesClassifier(n_estimators=200)
    else:
        print("specify valid classifier")
        exit()

    model = cal.CalibratedClassifierCV(clf,cv=params['cv'],method=params['method'])
    model.fit(X,y)
    
    return model

# Save the model
#def save_model(vectorizer,clf):
def save_model(model):    
    print('Saving the model')
    print('model: %s' %cfg.MODEL_FILE)
    #print('vectorizer: %s' %cfg.VECTORIZER_FILE)
    #jl.dump(vectorizer,cfg.VECTORIZER_FILE)
    jl.dump(model,cfg.MODEL_FILE) 
   
texts,y=pp.readData(data_file)

Xtrain,Xtest,ytrain,ytest=train_test_split(texts,y,test_size=0.1,random_state=0)#train_test_split(X,y,test_size=0.1,random_state=0)

#grid_search(Xtrain,ytrain,svm.LinearSVC(max_iter=1e5))
#exit()

vectorizer=get_vectorizer(texts)
clf=train_model(vectorizer.transform(Xtrain),ytrain)

model = Pipeline([('vectorizer',vectorizer),('classifier',clf)])

#score info
df_scores = pd.DataFrame(columns=['precision','recall',
                                  'jaccard','F1','TP',
                                  'FP','TN','FN'])
discrete_preds = model.predict(Xtest)#clf.predict(Xtest)
preds = model.predict_proba(Xtest)[:,1]#clf.predict_proba(Xtest)[:,1]
confusion = confusion_matrix(ytest,discrete_preds)

test_ones = sum(confusion[1][:])
test_zeros = sum(confusion[0][:])

entry = dict({'precision':precision_score(ytest,discrete_preds),
              'recall':recall_score(ytest,discrete_preds),
              'jaccard':jaccard_score(ytest,discrete_preds),
              'F1':f1_score(ytest,discrete_preds),
              'TP':confusion[1][1]/test_ones,
              'FP':confusion[0][1]/test_zeros,
              'TN':confusion[0][0]/test_zeros,
              'FN':confusion[1][0]/test_ones}) 
df_scores = df_scores.append(entry,ignore_index=True)
df_scores["avg score"]=df_scores[['precision','recall','jaccard','F1','TP','TN']].values.mean(axis=1)

print('Scoring the model')
score=model.score(Xtest,ytest)
print('Score: %s'%score)
print(df_scores)

save_model(model)
print('Done')
