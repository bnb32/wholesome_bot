import websockets
import asyncio
import uuid
import json
from getInfo import getUserID
import sys
import Settings as cfg
import socket
import time
from Logging import Logging
from notify_run import Notify
from Moderation import Moderation
import random
from emoji import demojize
from pytz import timezone
from datetime import datetime,timedelta

notify = Notify()
mod = Moderation()
log = Logging()

ping_msg="PING :tmi.twitch.tv\r\n"
pong_out_msg="PONG :tmi.twitch.tv\r\n"
pong_in_msg="PONG tmi.twitch.tv"

def getValue(key,dictionary):
    if key in dictionary: 
        return dictionary[key]
    else:
        for k in dictionary:
            if key in k:
                return dictionary[k]
        return ''    

def dateTime():
    
    date_str = datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
    datetime_obj = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S.%f")
    datetime_obj_pst = datetime_obj.replace(tzinfo=timezone('US/Eastern')) - timedelta(hours=3)
    return datetime_obj_pst.strftime("%Y-%m-%d %H:%M:%S.%f")


class ircSocketClient():

    def __init__(self):
        self.last_ping=time.time()
        self.uri='irc.chat.twitch.tv'
        self.port=6667
        
        pass

    async def connect(self):
        log.printConnectInfo('**Trying to connect to IRC**')
        self.connection = socket.socket()
        self.connection.connect((self.uri,self.port))
        pwd = "PASS oauth:"+cfg.TOKEN+"\r\n"
        self.connection.send(pwd.encode("utf-8"))
        nick="NICK "+cfg.NICK+"\r\n"
        self.connection.send(nick.encode("utf-8"))
        chan="JOIN #"+cfg.CHAN+"\r\n"
        self.connection.send(chan.encode("utf-8"))
        
        Loading = True
        line="CAP REQ :twitch.tv/tags\r\n"
        self.connection.send(line.encode("utf-8"))
        line="CAP REQ :twitch.tv/commands\r\n"
        self.connection.send(line.encode("utf-8"))
        line="CAP REQ :twitch.tv/membership\r\n"
        self.connection.send(line.encode("utf-8"))
        while Loading:
            line=self.connection.recv(1024).decode("utf-8")
            if("End of /NAMES list" in line):
                Loading=True
            else:
                Loading=False
            time.sleep(0.1)
        log.printConnectInfo('**IRC Connection established. IRC-Client correcly connected**')
        
    async def receiveMessage(self):
        while True:
            try:
                line=self.connection.recv(1024).decode("utf-8")
                
                #keep connection
                if line==ping_msg:
                    self.connection.send(pong_out_msg.encode("utf-8"))
                    self.last_ping=time.time()
                elif pong_in_msg in line:
                   self.last_ping=time.time()
                
                else:
                    #get info on line
                    self.handleMessage(line)
            
            except:
                if time.time()>=(self.last_ping+300):
                    try:
                        self.connection.send(ping_msg.encode("utf-8"))
                    except:
                        log.printConnectInfo('**Lost IRC connection**')
                        notify.send("Chatbot disconnnected")
                        break

    def handleMessage(self,line):

        info=mod.getInfo(line)
        if mod.print_info:
            mod.printInfo(info)

        #response to line
        mod.sendReply(self.connection,info)
        mod.sendAction(self.connection,info)
                            
        log_entry = "<"
        if info.mod:
            log_entry+="@"
        if info.vip:
            log_entry+="!"
        if info.sub:
            log_entry+="%"
        log_entry += info.user+"> "
        log_entry += info.msg

        if info.deleted:
            log_entry += "\n"
            log_entry += "DELETED: %s" %info.user
            log_entry += " (%s)\n" %info.msg
            log_entry += "MOD_ACTION: %s" %cfg.NICK
            log_entry += " (delete %s" %info.user
            log_entry += " %s)" %info.msg

            print(log_entry)
        
        log.UserLog[info.user]=info.msg

        #write to log
        if log.write_log:
            try:
                log.appendLog(log_entry)
            except:
                print("\n**logging problem**")
                print(line)       

    async def heartbeat(self):
        while True:
            try:
                self.connection.send(ping_msg.encode("utf-8"))
                await asyncio.sleep(3600)
            except:
                print('Connection with server closed')
                break

class webSocketClient():

    def __init__(self):

        self.topics=["chat_moderator_actions.%s.%s" %(getUserID(cfg.NICK),getUserID(cfg.CHAN))]
        self.auth_token = cfg.TOKEN
        self.message = {}
        self.last_ping = time.time()
        self.last_pong = time.time()
        self.last_msg_time = time.time()
        self.ping_timeout = 60
        self.sleep_time = 60
        self.wait_time = 300
        self.uri = 'wss://pubsub-edge.twitch.tv'
        pass
    
    async def listenForever(self):
        while True:
            try:
                
                log.printConnectInfo('**Trying to connect to PubSub**')

                async with websockets.connect(self.uri) as self.connection: 
                    #self.connection = await websockets.client.connect(self.uri)    
                    if self.connection.open:
                        log.printConnectInfo('**PubSub Connection established. Web-Client correcly connected**')
                        message = {"type": "LISTEN", "nonce": str(self.generate_nonce()), "data":{"topics": self.topics, "auth_token": self.auth_token}}
                        json_message = json.dumps(message)
                        await self.sendMessage(json_message)

                        while True:
                            try:
                                log.printConnectInfo('**Waiting for PubSub message**')
                                message = await asyncio.wait_for(self.connection.recv(), timeout=self.wait_time)
                                self.last_msg_time = dateTime()
                                log.printConnectInfo('**Message received: %s**' %(self.last_msg_time))
                                

                            except: #(asyncio.TimeoutError, websockets.exceptions.ConnectionClosed):
                                try:
                                    
                                    self.last_ping = dateTime()
                                    log.printConnectInfo('**PubSub Ping: %s**' %(self.last_ping))
                                    pong = await self.connection.ping()
                                    await asyncio.wait_for(pong, timeout=self.ping_timeout)
                                    self.last_pong = dateTime()
                                    log.printConnectInfo('**PubSub Pong: %s**' %(self.last_pong))

                                    log.printConnectInfo('**Ping OK, keeping connection alive...**')
                                    
                                    continue
                                except:
                                    
                                    log.printConnectInfo('**PubSub Ping failed**')
                                    #await asyncio.sleep(self.sleep_time)
                                    break  # inner loop
                            # do stuff with reply object

                            self.message = message
                            self.handleMessage(message)
                                                        
            except socket.gaierror:
                print("\n**socket.gaierror**")
                # log something
                continue
            except ConnectionRefusedError:
                print("\n**ConnectionRefusedError**")
                # log something else
                continue
            
            except websockets.exceptions.ConnectionClosed:
                log.printConnectInfo('**Connection with server closed**')
                continue

    def handleMessage(self,message):
        tmp = json.loads(message)
        log.printConnectInfo(tmp)

        if tmp['type'] == 'PONG':
            self.last_pong = time.time()

        if tmp['type'] == 'MESSAGE':
            
            tmp = json.loads(tmp['data']['message'])
            tmp = tmp['data']
            
            action = getValue('moderation_action',tmp)
            mod = getValue('created_by',tmp)
            
            if 'args' in tmp:
                user = tmp['args'][0]
            else:
                user = getValue('target_user_login',tmp)

            if action in ['ban','timeout']:
                if user in log.UserLog:
                    msg = log.UserLog[user]
                else:
                    msg = ''
            if action=="timeout":
                secs=tmp['args'][1]
            if action=="delete":
                msg = demojize(tmp['args'][1])
                msg_id = tmp['args'][2]

            log_entry = ""
            if action=="ban":
                log_entry = "BAN: %s\n" %user
                log_entry += "MOD_ACTION: %s" %mod
                log_entry += " (%s %s %s)" %(action,user,msg)
            elif action=="timeout":
                log_entry = "BAN: %s (%ss)\n" %(user,secs)
                log_entry += "MOD_ACTION: %s" %mod
                log_entry += " (%s %s %s %s)" %(action,user,secs,msg)
            elif action=="delete":
                log_entry = "DELETED: %s (%s)\n" %(user,msg)
                log_entry += "MOD_ACTION: %s" %mod
                log_entry += " (%s %s" %(action,user)
                log_entry += " %s %s)" %(msg,msg_id)
            else:
                log_entry += "MOD_ACTION: %s" %mod
                log_entry += " (%s %s)" %(action,user)

            print("\n%s" %log_entry)    
            if log.write_log:
                try:
                    log.appendLog("%s" %(log_entry))
                except:
                    print("\n**logging problem**")

    def generate_nonce(self):
        nonce = uuid.uuid1()
        oauth_nonce = nonce.hex
        return oauth_nonce

    async def sendMessage(self, message):
        await self.connection.send(message)

    async def heartbeat(self):
        while True:
            try:
                data_set = {"type": "PING"}
                json_request = json.dumps(data_set)
                print(json_request)
                await connection.send(json_request)
                await asyncio.sleep(60)
            
            except websockets.exceptions.ConnectionClosed:
                print('\n**Connection with server closed**')
                break
