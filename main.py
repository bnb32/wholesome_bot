import string
import Moderation as mod
import Settings as cfg
import time
import Logging as log
from notify_run import Notify 
from SocketClient import webSocketClient, ircSocketClient
import asyncio
import threading
notify = Notify() 
      
def connectClient(client,loop):
    asyncio.set_event_loop(loop)
    loop.run_until_complete(client.connect())
    return

def runClient(client,loop):
     
    asyncio.set_event_loop(loop)
    tasks = [
        asyncio.ensure_future(client.heartbeat()),
        asyncio.ensure_future(client.receiveMessage()),
    ]
    loop.run_until_complete(asyncio.wait(tasks))
    return

if __name__ == '__main__':
    
    ircClient = ircSocketClient()
    webClient = webSocketClient()
    
    ircLoop = asyncio.new_event_loop()
    webLoop = asyncio.new_event_loop()

    connectClient(ircClient,ircLoop)
    ircThread = threading.Thread(target=runClient,args=(ircClient,ircLoop))
    ircThread.start() 
    
    #asyncio.set_event_loop(webLoop)
    #tasks = [
    #         asyncio.ensure_future(webClient.listenForever()),
    #         asyncio.ensure_future(webClient.heartbeat()),
    #        ]
    #webLoop.run_until_complete(asyncio.wait(tasks))

    asyncio.run(webClient.listenForever())
