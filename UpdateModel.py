import argparse
import os
import glob
import Settings as cfg

parser=argparse.ArgumentParser(description="Update model with new data")
parser.add_argument('-infile', required=True, type=str)
parser.add_argument('-append', default=False, action='store_true')
parser.add_argument('-train', default=False, action='store_true')
parser.add_argument('-rerun', default=False, action='store_true')
args=parser.parse_args()

PWD=os.getcwd()
LOG_FILES=glob.glob('%s/*%s.log' %(cfg.CHATTY_DIR,cfg.CHAN))
RAW_LOG="%s/data/%s.log" %(PWD,cfg.CHAN)
TMP="%s/data/clean_tmp.txt" %(PWD)
T_IN="%s/data/%s_data.csv" %(PWD,cfg.CHAN)
T_OUT="%s/data/%s_data_trim.csv" %(PWD,cfg.CHAN)

if not args.append and not args.rerun:
    T_IN=args.infile

if args.append:    
    os.system('python3 CleanLog.py -clean -infile %s -outfile %s' %(args.infile,TMP))
    os.system('tail -n +2 %s >> %s' %(TMP,T_IN))
    print('Appended %s to %s' %(TMP,T_IN))

elif args.rerun:
    print('Building raw log: %s' %(RAW_LOG))
    os.system('rm -f %s' %(RAW_LOG))
    os.system('cat %s >> %s' %(LOG_FILES,RAW_LOG))
    os.system('python3 CleanLog.py -clean -infile %s -outfile %s' %(args.infile,TMP))
    os.system('cp %s %s' %(TMP,T_IN))
    
if args.train:    
    os.system('python3 %s/CleanLog.py -trim -infile %s -outfile %s' %(PWD,T_IN,T_OUT))
    os.system('python3 %s/Training.py -model 1 -vectorizer 1 -dataset %s' %(PWD,T_OUT))
