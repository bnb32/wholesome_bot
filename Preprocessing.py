import pandas as pd
import re
import nltk
from nltk.corpus import stopwords
stop_words=set(stopwords.words('english'))
from nltk.stem import WordNetLemmatizer
wordnet_lemmatizer=WordNetLemmatizer()
import clean_info as cinfo
import random
import pkg_resources
from symspellpy import SymSpell, Verbosity
import time
from collections import defaultdict
from emotes import emotes
import progressbar
from emoji import demojize
import Settings as cfg

sym_spell=SymSpell(max_dictionary_edit_distance=2,prefix_length=7)
sym_spell_seg=SymSpell(max_dictionary_edit_distance=0,prefix_length=7)
dictionary_path=pkg_resources.resource_filename("symspellpy","frequency_dictionary_en_82_765.txt")
bigram_path=pkg_resources.resource_filename("symspellpy","frequency_bigramdictionary_en_243_342.txt")
sym_spell.load_dictionary(dictionary_path,term_index=0,count_index=1)
sym_spell_seg.load_dictionary(dictionary_path,term_index=0,count_index=1)
sym_spell.load_bigram_dictionary(bigram_path,term_index=0,count_index=2)

# Read in data
def readData(dfile):
    print('Reading in data: %s'%dfile)
    data = pd.read_csv(dfile)
    texts = data['text'].astype(str)
    y = data['is_offensive']
    return texts,y

def correctMsg(line):
    
    tmp=line#filterEmotes(line)
    tmp=delUsernames(tmp)
    tmp=pruneChars(tmp)#re.sub('[^A-Za-z0-9 ]+','',tmp)
    tmp=removeReps(tmp,sym_spell.words)
    return tmp

def segmentWords(line):
    if line!="":
        return sym_spell_seg.word_segmentation(line.strip()).segmented_string
    else:
        return line

def my_tokenizer(s):    
    words=s.lower().split()
    words=[pruneChars(w) for w in words if w not in stop_words]
    return my_lemmatizer(words)

def remove_stopwords(words):
    return [word for word in words if word not in stop_words]

def my_lemmatizer(words):
    return [wordnet_lemmatizer.lemmatize(w) for w in words]

def preprocWords(texts):
    return [my_tokenizer(text) for text in texts]

def joinWords(lines):
    return [' '.join(line) for line in lines]

def filterEmotes(line):
    tmp=line
    for emote in emotes:
        tmp=re.sub(emote,'',tmp,flags=re.I)
    return tmp.rstrip().lstrip()

def filterAllEmotes(texts,y):
    ttmp=[]
    ytmp=[]
    for n,text in enumerate(texts):
        tmp=filterEmotes(text)
        if tmp.strip()!='':
            ttmp.append(tmp)
            ytmp.append(y[n])
    return ttmp,ytmp

def writeData(ofile,texts,y):    
    f=open(ofile,'w')
    print("Writing data: %s"%ofile)
    f.write('is_offensive,text\n')
    for n,t in enumerate(texts):
        if re.sub('[^A-Za-z]+','',t)!="":
            if int(y[n])==1:
                f.write('%s,"%s"\n'%(y[n],t))
    for n,t in enumerate(texts):
        if re.sub('[^A-Za-z]+','',t)!="":
            if int(y[n])==0:
                f.write('%s,"%s"\n'%(y[n],t))
    f.close()    

def delUsernames(line):
    msg=re.sub(r'@\w+', '',line)
    return msg

def removeReps(line,word_list):
    p=re.compile(r'(.)\1{1,}')
    words=p.sub(r'\1\1',line.lower()).split()
    for n,w in enumerate(words):
        if re.sub('[^A-Za-z0-9]+','',w) not in word_list:
            tmp=p.sub(r'\1',w)
            if re.sub('[^A-Za-z0-9]+','',tmp) in word_list:
                words[n]=tmp

    return ' '.join(words)

def pruneChars(line):
    return re.sub('[^A-Za-z0-9_?#*!/-@ ]+','',line.rstrip('\n').rstrip().lstrip())

def chkMsgs(line,checks):
    for cp in checks:
        pattern=r'\b{}*\?*\b'.format(cp+cp[-1])
        if re.search(pattern,line,re.I):
            return True
    return False

def trimLog(texts,y,frac):
    tnew=[]
    ynew=[]
    for n,val in enumerate(y):
        if val==0:
            choice=random.choices([False,True],weights=[1-frac,frac])[0]
            if choice:
                ynew.append(y[n])
                tnew.append(texts[n])
        elif val==1:
            ynew.append(y[n])
            tnew.append(texts[n])
    return tnew,ynew

def containsLink(text):
    if any(l in text for l in cinfo.lstrings):
        return True
    else:
        return False
    
class LogCleaning:
    def __init__(self,rfile,cfile,wc):
        self.rfile=rfile
        self.cfile=cfile
        self.lines=None
        self.mem=msgMemory()
        self.wc = wc

    def isValidLine(self,line):
        if line.startswith(tuple(cinfo.starts+['<'])):
            return True
        else:
            return False

    def readLog(self):
        #read raw log
        print("Reading log: %s"%(self.rfile))
        fr=open(self.rfile)
        lines=fr.readlines()
        fr.close()
        lines=[l.lstrip() for l in lines]
        return lines

    def prepLog(self): 
        #prep log
        self.lines=self.readLog()
        return [delUsernames(re.sub('"','\'',l)).rstrip('\n').rstrip().lstrip() for l in self.lines if self.isValidLine(l)]
   
    def cleanLog(self):
        #prep log
        lines=self.prepLog()
        self.mem.buildFullMemory(lines)
        self.mem.chunkMemory()
        memory=self.mem.chunks

        cmsgs=[]
        bmsgs=[]
        tocheck=[]
        ctmp=[]

        banned_user_count=0
        clean_user_count=0

        print("Dividing messages into 'bad' and 'other' for each user")
        with progressbar.ProgressBar(max_value=len(memory)) as bar:
            for n,user in enumerate(memory):
                bar.update(n)
                if user not in cinfo.ignore_users:

                    if any(m['banned'] for m in memory[user]):
                        banned_user_count+=1
                    else:
                        clean_user_count+=1

                    for m in memory[user]:
                        if (m['sub']==False or m['sub']==True) and not containsLink(m['msg']) :
                                            
                            for w in cinfo.whitelist:
                                m['msg'] = re.sub(w,'',m['msg'],flags=re.I)

                            if (m['banned'] or m['deleted']) and m['mod'] in cinfo.ignore_actions: 
                                pass

                            elif m['banned'] and m['mod'] in cinfo.ban_checks:
                                tocheck.append(m['msg'])
                            elif m['deleted'] and m['mod'] in cinfo.delete_checks:
                                tocheck.append(m['msg'])
                            elif m['deleted'] and m['mod']==None:
                                tocheck.append(m['msg'])
                            elif m['deleted']:    
                                bmsgs.append(m['msg'])
                            elif m['banned']:
                                bmsgs.append(m['msg'])
                            else:
                                ctmp.append(m['msg'])

        print("Banned/timed-out users: %s" %(banned_user_count))
        print("Clean users: %s" %(clean_user_count))

        print("Dividing 'other' into clean, to-check, and bad") 
        with progressbar.ProgressBar(max_value=len(ctmp)) as bar:
            for n,text in enumerate(ctmp):
                bar.update(n)
                                
                if self.wc.predict_prob([text])[0]>cfg.CHECK_PMIN:
                    tocheck.append(text)
                elif chkMsgs(text.lower(),cinfo.blacklist):
                    bmsgs.append(text)
                elif chkMsgs(text.lower(),cinfo.graylist):
                    tocheck.append(text)
                else:
                    cmsgs.append(text)

        texts=[]
        y=[]
        
        print("Appending to-check messages: %s"%(len(tocheck)))
        with progressbar.ProgressBar(max_value=len(tocheck)) as bar:
            for n,text in enumerate(tocheck):
                
                bar.update(n)

                if chkMsgs(text.lower(),cinfo.blacklist):
                    rating=1
                else:    
                    if text.lower() in (bmsg.lower() for bmsg in bmsgs): 
                        rating=1
                    elif text.lower() in (cmsg.lower() for cmsg in cmsgs): 
                        rating=0
                    else:
                        rating=input('\nCheck: '+text+'\n')
                if rating not in ['0','1','s']:
                    rating=0
                if rating=='s':
                    print("Skipped: %s"%text)
                else:
                    if rating=='1':
                        bmsgs.append(text)

                    if rating=='0':
                        cmsgs.append(text)
        
        print("Appending banned messages: %s" %(len(bmsgs)))
        with progressbar.ProgressBar(max_value=len(bmsgs)) as bar:
            for n,text in enumerate(bmsgs):
                #if text not in texts:
                texts.append(text)
                y.append('1')
                
                bar.update(n)

        print("Appending clean messages: %s" %(len(cmsgs)))
        with progressbar.ProgressBar(max_value=len(cmsgs)) as bar:
            for n,text in enumerate(cmsgs):
                #if text not in texts:
                texts.append(text)
                y.append('0')

                bar.update(n)

        #print("Filtering emotes")
        #texts,y=filterAllEmotes(texts,y)
        writeData(self.cfile,texts,y)

class msgMemory:
    def __init__(self):
        self.memory=defaultdict(list)
        self.chunks=defaultdict(list)
        self.msg_limit=1

    def addMsg(self,user,msg,sub=False,vip=False,banned=False,deleted=False,mod=None):
        entry={'msg': demojize(msg), 'sub': sub, 'vip': vip, 'banned': banned, 'deleted': deleted, 'mod': mod}
        self.memory[user].append(entry)
        return self

    def delMsg(self,user):        
        self.memory[user].pop(0)
        return self
    
    def checkMsgs(self,user):
        if len(self.memory[user])>0:
            if self.memory[user][-1]['banned']:
                self.clearUser(user)
            elif len(self.memory[user])>self.msg_limit:
                self.delMsg(user)
        return self 

    def updateUserBan(self,user,banned=True):
        self.memory[user][-1]['banned']=banned
        return self    
    
    def clearUser(self,user):
        self.memory[user]=[]
        return self

    def buildFullMemory(self,lines):
        for l in lines:
            if l.startswith('<'):
                user=l.split('>')[0]
                user=user.strip('<')
                msg=l[l.index('>')+1:].lstrip()
                subbed=False
                if '%' in user: subbed=True
                user=re.sub('[^A-Za-z0-9_]+','',user).lower()
                self.addMsg(user,msg,sub=subbed)
            elif l.startswith('BAN:'):
                try:
                    user=l.split()[1].lower()
                    self.memory[user][-1]['banned']=True
                except: pass
            elif l.startswith('DELETED:'):
                try:
                    user=l.split()[1].lower()
                    self.memory[user][-1]['deleted']=True
                except: pass
            elif l.startswith('MOD_ACTION:'):
                try:
                    user=l.split()[3].lower()
                    mod=l.split()[1].lower()
                    self.memory[user][-1]['mod']=mod
                except: pass
        return self
    
    def chunkMemory(self):
        for user in self.memory:
            msgs=self.memory[user].copy()
            count=0
            for m in msgs:
                if count==0:
                    msg=""
                    mod=None
                    banned=False
                    deleted=False
                    subbed=False
                    vip=False

                msg+=m['msg']+'. '
                if m['sub']: subbed=True
                if m['vip']: vip=True
                if m['banned']: banned=True
                if m['deleted']: deleted=True
                if m['mod']!=None: mod=m['mod']
                self.memory[user].pop(0)
                count+=1

                if count==self.msg_limit or banned or deleted or len(self.memory[user])==0:
                    self.chunks[user].append({'msg':msg,'sub':subbed,'vip':vip,'banned':banned,'deleted':deleted,'mod':mod})
                    count=0
        return self 

    def chunkRecent(self,info):
        self.checkMsgs(info.user)
        self.addMsg(info.user,info.msg,sub=info.sub,vip=info.vip)
        msgs=[m['msg'].strip('\r') for m in self.memory[info.user][-self.msg_limit:]]
        return '. '.join(msgs)   

def checkProbs(ifile,ofile,bounds,wc):
    tmp_texts,tmp_y=readData(ifile)
    texts=[];y=[]
    to_check=[];y_check=[]
    
    print("Separating to_check from all data")
    with progressbar.ProgressBar(max_value=len(tmp_texts)) as bar:
        for i,t in enumerate(tmp_texts):
            t = demojize(t)
            if bounds[0]<=i<=bounds[1] and wc.predict_prob([t])[0]>0.1 and tmp_y[i]==0 and not containsLink(t):
                to_check.append(t)
                y_check.append(tmp_y[i])
            else:
                texts.append(t)
                y.append(tmp_y[i])
            bar.update(i)        
            
    print("Reclassifying to_check messages")        
    with progressbar.ProgressBar(max_value=len(to_check)) as bar:
        end_check = False
        for i,t in enumerate(to_check):        
            
            if not end_check:
                bar.update(i)    
                rating=input('\nCheck: %s, %s'%(y_check[i],t))
                if rating not in ['0','1','s','e']:
                    rating=y_check[i]
                elif rating=='s':
                    print("Skipped: %s"%t)
                elif rating=='e':
                    end_check = True
                else:
                    texts.append(t)
                    y.append(rating)
            else:
                for j in range(i,len(to_check)):
                    texts.append(to_check[j])
                    y.append(y_check[j])

    writeData(ofile,texts,y)

def checkPhrases(ifile,ofile):
    tmp_texts,tmp_y=readData(ifile)
    phrases=input('Enter phrases to check\n')
    phrases=phrases.split(',')   
    texts=[];y=[]
    to_check=[];y_check=[]
    
    print("Separating to_check from all data")
    with progressbar.ProgressBar(max_value=len(tmp_texts)) as bar:
        for i,t in enumerate(tmp_texts):
            if chkMsgs(t.lower(),phrases):
                to_check.append(t)
                y_check.append(tmp_y[i])
            else:
                texts.append(t)
                y.append(tmp_y[i])
            bar.update(i)        
            
    print("Reclassifying to_check messages")        
    with progressbar.ProgressBar(max_value=len(to_check)) as bar:
        end_check = False
        for i,t in enumerate(to_check):        
            
            if not end_check:
                bar.update(i)    
                rating=input('\nCheck: %s, %s'%(y_check[i],t))
                if rating not in ['0','1','s','e']:
                    rating=y_check[i]
                elif rating=='s':
                    print("Skipped: %s"%t)
                elif rating=='e':
                    end_check = True
                else:
                    texts.append(t)
                    y.append(rating)
            else:
                for j in range(i,len(to_check)):
                    texts.append(to_check[j])
                    y.append(y_check[j])

    writeData(ofile,texts,y)

