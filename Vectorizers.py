from gensim.models import Word2Vec
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
import numpy as np
import sklearn.feature_extraction.text as ft
from collections import defaultdict
import Preprocessing as pp
import itertools
#from glove import Corpus, Glove

#word2vecs based vectorizer
class VectorizerW2V(object):
    def __init__(self, vparams):
        self.w2v=None
        self.wparams=wparams
        self.model=None
        self.dim=0

    def buildModel(self,texts):
        words=pp.preprocWords(texts)
        self.model=Word2Vec(words,**self.wparams)
        return self
    
    def buildW2V(self,texts):
        self.buildModel(texts)
        self.w2v={w: v for w, v in zip(self.model.wv.index2word,self.model.wv.syn0)}
        if len(self.w2v)>0:
            self.dim = len(next(iter(self.w2v.values())))
        return self

    def updateW2V(self,text):
        for word in text:
            self.w2v[word]=self.model.wv[word]
        return self        
    
    def updateModel(self,text):
        #words=pp.preprocWords(texts)
        self.model.build_vocab([text],update=True)
        self.model.train([text],total_examples=1,epochs=1)
        self.updateW2V(text)
        return self

    def fit(self,X,y=None):
        return self

    def transform(self,X):
        for words in pp.preprocWords(X):
            if not all(w in self.w2v for w in words): 
                self.updateModel(words)
        return np.array([np.mean([self.w2v[w] for w in words if w in self.w2v] or [np.zeros(self.dim)], axis=0) for words in pp.preprocWords(X)]) 
    
    def fit_transform(self,X,y=None):
        self.fit(X,y)
        return np.array([np.mean([self.w2v[w] for w in words if w in self.w2v] or [np.zeros(self.dim)], axis=0) for words in pp.preprocWords(X)])

class TfidfVectorizerW2V(VectorizerW2V):
    def __init__(self, vparams, wparams):
        self.w2v=None
        self.vparams=vparams
        self.wparams=wparams
        self.w2wt = None
        self.model=None
        self.dv = None
        self.tfidf = None
        self.dim=0
    
    def fit(self,X,y=None):
        self.buildW2V(X)
        self.tfidf = ft.TfidfVectorizer(**self.vparams)
        self.tfidf.fit(X)
        self.dv = max(self.tfidf.idf_)
        self.w2wt=dict(zip(self.tfidf.get_feature_names(),self.tfidf.idf_))
        return self

    def transform(self,X):
        for words in pp.preprocWords(X):
            if not all(w in self.w2v for w in words): 
                self.updateModel(words)
        return np.array([np.mean([self.w2v[w]*self.w2wt.setdefault(w,self.dv) for w in words if w in self.w2v] or [np.zeros(self.dim)], axis=0) for words in pp.preprocWords(X)]) 
    
    def fit_transform(self,X,y=None):
        self.fit(X,y)
        return np.array([np.mean([self.w2v[w]*self.w2wt.setdefault(w,self.dv) for w in words if w in self.w2v] or [np.zeros(self.dim)], axis=0) for words in pp.preprocWords(X)]) 

#doc2vecs based vectorizer
class VectorizerD2V:
    def __init__(self,dparams):
        self.dparams=dparams
        self.d2v=None
        self.model=None


    def buildDocs(self,texts):
        docs=[TaggedDocument(doc,[i]) for i, doc in enumerate(texts)]
        return docs
    
    def buildModel(self,texts):
        docs=pp.preprocWords(texts)
        self.model=Doc2Vec(self.buildDocs(docs),**self.dparams)
        return self
    
    def buildD2V(self,texts):
        self.buildModel(texts)
        self.d2v={}
        for t,v in enumerate(self.model.docvecs.doctag_syn0):
            self.d2v[t]=v
        return self
    
    def updateD2V(self,text):
        return self        
    
    def updateModel(self,text):
        return self

    def fit(self,X,y=None):
        self.buildD2V(X)
        return self

    def transform(self,X):
        return np.array([self.model.infer_vector(words)] for words in pp.preprocWords(X)) 
    
    def fit_transform(self,X,y=None):
        self.fit(X,y)
        return np.array([self.model.infer_vector(words)] for words in pp.preprocWords(X)) 

#glove based vectorizer
class VectorizerGlove(object):
    def __init__(self, gparams):
        self.w2g=None
        self.gparams=gparams
        self.model=None
        self.dim=0
        self.corpus=Corpus()

    def buildModel(self,texts):
        words=pp.preprocWords(texts)
        self.corpus.fit(words,window=self.gparams['window'])
        self.model=Glove(no_components=self.gparams['no_components'],learning_rate=self.gparams['learning_rate'])
        self.model.fit(self.corpus.matrix,epochs=self.gparams['epochs'],no_threads=self.gparams['no_threads'])
        self.model.add_dictionary(self.corpus.dictionary)
        return self
    
    def buildGlove(self,texts):
        self.buildModel(texts)
        self.w2g={}
        for w in self.model.dictionary:
            self.w2g[w]=self.model.word_vectors[self.model.dictionary[w]]
        #self.w2g={w: v for w, v in zip(self.model.word_vectors,self.model.dictionary)}
        if len(self.w2g)>0:
            self.dim = len(next(iter(self.w2g.values())))
        return self

    def updateGlove(self,text):
        for word in text:
            self.w2g[word]=self.model.word_vectors[self.model.dictionary[word]]
        return self        
    
    def updateModel(self,text):
        #words=pp.preprocWords(texts)
        #self.model.build_vocab([text],update=True)
        #self.model.train([text],total_examples=1,epochs=1)
        #self.updateGlove(text)
        return self

    def fit(self,X,y=None):
        self.buildGlove(X)
        return self

    def transform(self,X):
        for words in pp.preprocWords(X):
            if not all(w in self.w2g for w in words): 
                self.updateModel(words)
        return np.array([np.mean([self.w2g[w] for w in words if w in self.w2g] or [np.zeros(self.dim)], axis=0) for words in pp.preprocWords(X)]) 
    
    def fit_transform(self,X,y=None):
        self.fit(X,y)
        return np.array([np.mean([self.w2g[w] for w in words if w in self.w2g] or [np.zeros(self.dim)], axis=0) for words in pp.preprocWords(X)])

class TfidfVectorizerGlove(VectorizerGlove):
    def __init__(self,vparams,gparams):
        self.w2g=None
        self.gparams=gparams
        self.vparams=vparams
        self.model=None
        self.dim=0
        self.corpus=Corpus()
        
    def fit(self,X,y=None):
        self.buildGlove(X)
        self.tfidf = ft.TfidfVectorizer(**self.vparams)
        self.tfidf.fit(X)
        self.dv = max(self.tfidf.idf_)
        self.w2wt=dict(zip(self.tfidf.get_feature_names(),self.tfidf.idf_))
        return self

    def transform(self,X):
        for words in pp.preprocWords(X):
            if not all(w in self.w2g for w in words): 
                self.updateModel(words)
        return np.array([np.mean([self.w2g[w]*self.w2wt.setdefault(w,self.dv) for w in words if w in self.w2g] or [np.zeros(self.dim)], axis=0) for words in pp.preprocWords(X)]) 
    
    def fit_transform(self,X,y=None):
        self.fit(X,y)
        return np.array([np.mean([self.w2g[w]*self.w2wt.setdefault(w,self.dv) for w in words if w in self.w2g] or [np.zeros(self.dim)], axis=0) for words in pp.preprocWords(X)]) 

 
