TOKEN= #oauth token
NICK= #bot username
CHAN= #channel to moderate
PLEB_PMIN=0.55 #threshold probability to delete pleb msgs
SUB_PMIN=0.65 #threshold prob for subs
CHECK_PMIN=0.2 #threshold prob to check for classification
PMSG_ON=False #send probability message in chat
TO_SUBS=True #timeout subs also
PRINT_INFO=True #print info in terminal
WRITE_LOG=True #write chat data to log
NOLINKS=False #bot should also timeout links
CLIENT_ID= #client id for bot
CLIENT_SECRET= #client secret for bot
MODEL_FILE='./data/model.joblib'
CHATTY_DIR="/home/bbent/win_home/.chatty/logs"
MULTIPLIER=25 # ratio of zeros/ones for training data
