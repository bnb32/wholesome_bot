import socket,string,time
from notify_run import Notify
from Settings import *
notify = Notify()
last_ping = time.time()

ping_msg="PING :tmi.twitch.tv\r\n"
pong_out_msg="PONG :tmi.twitch.tv\r\n"
pong_in_msg="PONG tmi.twitch.tv"

HOST = "irc.chat.twitch.tv" #host
PORT = 6667

def openSocket():
	
    s = socket.socket()
    s.connect((HOST,PORT))
    pwd="PASS oauth:"+TOKEN+"\r\n"
    s.send(pwd.encode("utf-8"))
    nick="NICK "+NICK+"\r\n"
    s.send(nick.encode("utf-8"))
    chan="JOIN #"+CHAN+"\r\n"
    s.send(chan.encode("utf-8"))
    return s

def closeSocket(s):  
    chan="PART #"+CHAN+"\r\n"
    s.send(chan.encode("utf-8"))
    s.shutdown(socket.SHUT_WR)
    s.close()

def restartSocket(s):
    closeSocket(s)
    snew=openSocket()
    joinRoom(snew)
    return snew

def sendMessage(s, message):
    messageTemp="PRIVMSG #"+CHAN+" :"+message
    tmp=messageTemp+"\r\n"
    s.send(tmp.encode("utf-8"))
    print("Sent: " + messageTemp)

def joinRoom(s):
    Loading = True
    line="CAP REQ :twitch.tv/tags\r\n"
    s.send(line.encode("utf-8"))
    line="CAP REQ :twitch.tv/commands\r\n"
    s.send(line.encode("utf-8"))
    line="CAP REQ :twitch.tv/membership\r\n"
    s.send(line.encode("utf-8"))
    while Loading:
        line=s.recv(1024).decode("utf-8")
        print(line)
        Loading = loadingComplete(line)
        time.sleep(0.1)
    print("Successfully joined chat")
	
def loadingComplete(line):
	  if("End of /NAMES list" in line):
		    return False
	  else:
		    return True

