from pynput import keyboard as kb
import Socket as sock

class KeyHandling:
    def __init__(self,mod,socket):
        self.s=socket
        self.break_program=False
        self.current=set()
        self.mod=mod
        self.COMBOS={
        'youtube_allowed':[kb.Key.alt_r,kb.KeyCode.from_char('y')], 
        'urls_allowed':[kb.Key.alt_r,kb.KeyCode.from_char('u')], 
        'prob_up':[kb.Key.alt_r,kb.Key.page_up], 
        'prob_down':[kb.Key.alt_r,kb.Key.page_down],
        'prob_msg':[kb.Key.alt_r,kb.KeyCode.from_char('p')],
        'print_info':[kb.Key.alt_r,kb.KeyCode.from_char('i')],
        'write_log':[kb.Key.alt_r,kb.KeyCode.from_char('l')],
        'check_joins':[kb.Key.alt_r,kb.KeyCode.from_char('c')],
        'mem_length_up':[kb.Key.alt_r,kb.KeyCode.from_char('m')],
        'mem_length_down':[kb.Key.alt_r,kb.KeyCode.from_char('k')],
        'send_message':[kb.Key.alt_r,kb.KeyCode.from_char('s')],
        'nuke':[kb.Key.alt_r,kb.KeyCode.from_char('n')],
        }

    def check_combo(self,key,combos):
        if key in combos:
            self.current.add(key)
            if all(k in self.current for k in combos):
                return True
        return False

    def on_press(self,key):
        if key == kb.Key.end:
            print ('end pressed')
            self.break_program = True
            return False
        elif self.check_combo(key,self.COMBOS['youtube_allowed']):
            self.mod.allow_youtube=not(self.mod.allow_youtube)
            print('Allow youtube links: %s'%self.mod.allow_youtube)
            #print('%s pressed'%key)
        elif self.check_combo(key,self.COMBOS['prob_up']):  
            self.mod.pleb_prob+=0.01
            print('Timeout threshold: %s'%self.mod.pleb_prob)
        elif self.check_combo(key,self.COMBOS['prob_down']):
            self.mod.pleb_prob-=0.01
            print('Timeout threshold: %s'%self.mod.pleb_prob)
        elif self.check_combo(key,self.COMBOS['prob_msg']):
            self.mod.send_pmsg=not(self.mod.send_pmsg)
            print('Send probability message: %s'%self.mod.send_pmsg)
        elif self.check_combo(key,self.COMBOS['print_info']):
            self.mod.print_info=not(self.mod.print_info)
            print('Print info: %s'%self.mod.print_info)
        elif self.check_combo(key,self.COMBOS['check_joins']):
            self.mod.send_join_msg=not(self.mod.send_join_msg)
            print('Send join message: %s'%self.mod.send_join_msg)
        elif self.check_combo(key,self.COMBOS['write_log']):
            self.mod.write_log=not(self.mod.write_log)
            print('Write to log: %s'%self.mod.write_log)
        elif self.check_combo(key,self.COMBOS['urls_allowed']):
            self.mod.nolinks=not(self.mod.nolinks)
            print('Links removed: %s'%self.mod.nolinks)
        elif self.check_combo(key,self.COMBOS['mem_length_up']):
            self.mod.mem.msg_limit+=1
            print('Memory length: %s'%self.mod.mem.msg_limit)
        elif self.check_combo(key,self.COMBOS['mem_length_down']):
            self.mod.mem.msg_limit-=1
            print('Memory length: %s'%self.mod.mem.msg_limit)
        elif self.check_combo(key,self.COMBOS['send_message']):
            sock.pyramid(self.s)
        elif self.check_combo(key,self.COMBOS['nuke']):
            self.mod.nuke_test(self.s)
        else:
            pass
            #print("pressed: %s"%key)
    
    def on_release(self,key):
        try:
            self.current.remove(key)
        except KeyError:
            pass
