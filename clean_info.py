from phrase_lists import blacklist, whitelist, graylist

starts=['BAN:','MOD_ACTION:','DELETED:']
ignore_actions=['moobot']
ban_checks=[
            #'emadtreika',
            #'untike',
            #'gerberbaby3',
            'drchessgremlin',
            #'ijulia',
            #'1silvana1',
            #'rossana29',
            #'xryab',
            #'dr_bob_ross',

            ]
delete_checks=[
               #'emadtreika',
               #'untike',
               #'gerberbaby3',
               'drchessgremlin',
               #'ijulia',
               #'1silvana1',
               #'rossana29',
               #'xryab',
               #'dr_bob_ross',

               ]
ignore_users=[
              #'direftp',
              'chileaned',
              'drchessgremlin',
              'gerberbaby3',
              'veelaallure',
              'emadtreika',
              'agent_jl_chess',
              'hipside',
              'chessbae94',
              'crazycoffeeman',
              'joebruin',
              'moobot',
              '1silvana1',
              'untike',
              'rossana29',
              'xryab',
              'ijulia',
              'dr_bob_ross',
              'carloszma',
              'snowiefr',
              'cesarzma',
               ]
lstrings=['https://',
          'http://',
          'www.',
          '.com',
          '.net',
          '.org',
          '.edu',
          '/tv',
          '.tv',
          ]

